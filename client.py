import socket
import select
import msvcrt
import sys

name = raw_input("please enter your name: ")

client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client_socket.connect(('127.0.0.1', 8889))

message = ""

messages = []

my_name = '{0}: '.format(name)

sys.stdout.write("\r" + my_name + message)


def write_on_screen():
    pass


def read_input():
    global message
    if msvcrt.kbhit():
        letter = msvcrt.getch()
        message += letter
        sys.stdout.flush()
        sys.stdout.write("\r" + my_name + message)
        if letter == "\r":
            messages.append(message)
            sys.stdout.flush()
            print my_name + message
            message = ""
            sys.stdout.write("\r" + my_name + message)
            return


while True:
    rlist, wlist, xlist = select.select([client_socket], [client_socket], [])
    read_input()
    if client_socket in rlist:
        response = client_socket.recv(1024)
        sys.stdout.flush()
        print response
        sys.stdout.write("\r" + my_name + message)

    if client_socket in wlist:
        for every_message in messages:
            client_socket.send(every_message)
            messages.remove(every_message)
