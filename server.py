import socket
import select

server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_socket.bind(('0.0.0.0', 8889))
server_socket.listen(10)
open_client_sockets = []
messages_to_send = dict()


def send_waiting_messages(wlist):
    for every_client in wlist:
        if every_client in messages_to_send.keys():
            for every_message in messages_to_send[every_client]:
                every_client.send(every_message)
                messages_to_send[every_client].remove(every_message)


def add_the_message(message, sender):
    for every_client in open_client_sockets:
        if every_client != sender:
            if every_client in messages_to_send.keys():
                messages_to_send[every_client] += [message]
            else:
                messages_to_send[every_client] = [message]


while True:
    rlist, wlist, xlist = select.select([server_socket] + open_client_sockets, open_client_sockets, [])
    for current_socket in rlist:
        if current_socket is server_socket:
            new_socket, _ = server_socket.accept()
            print("Connection with client started.")
            open_client_sockets.append(new_socket)
        else:
            data = current_socket.recv(10000)
            if data == "":
                open_client_sockets.remove(current_socket)
                messages_to_send.pop(current_socket, None)
                print("Connection with client closed.")
            else:
                add_the_message(data, current_socket)
    send_waiting_messages(wlist)